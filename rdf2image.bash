#! /usr/bin/env bash

if [ $# -ne "2" -a $# -ne "4" ]  # Script invoked with bad number of args
then
  echo "Usage: `basename $0` options (-rp)"
  exit 1        # Exit and explain usage, if no argument(s) given.
fi  
# Usage: scriptname -options
# Note: dash (-) necessary

rdfFilePath=""
pictureFilePath=""

while getopts ":r:p:" Option
do
  case $Option in
    r     ) echo "Scenario #1: option -r- ${OPTARG}  [OPTIND=${OPTIND}]"; rdfFilePath=${OPTARG};;
    p     ) echo "Scenario #3: option -p- ${OPTARG}  [OPTIND=${OPTIND}]"; pictureFilePath=${OPTARG};;
    \? ) echo "Usage: $0 -r <RDF file path> [-p <Picture file path>]";;
  esac
done

shift $(($OPTIND - 1))
#  Decrements the argument pointer so it points to next argument.
#  $1 now references the first non option item supplied on the command line
#+ if one exists.

fileExtension=${rdfFilePath##*.}
case ${fileExtension} in
  owl  ) rdfFileType="rdfxml";;
  nt	) rdfFileType="ntriples";;
  ttl   ) rdfFileType="turtle";;
  trig  ) rdfFileType="trig";;
  \?    ) echo "unknown RDF file type"; exit 2 ;;
esac


if [[ ${pictureFilePath} == "" ]]
then
  pictureFilePath="${rdfFilePath%.*}.png"
fi

pictureFileType=${pictureFilePath##*.}

dotFilePath="${pictureFilePath%.*}.dot"

#rapper --input turtle ${rdfFilePath} --output dot > ${dotFilePath}
#dot -Tpng -o ${pictureFilePath} ${dotFilePath} 
#rapper --input turtle ${rdfFilePath} --output dot | dot -Tpng -o ${pictureFilePath}
rapper --input ${rdfFileType} ${rdfFilePath} --output dot | dot -T${pictureFileType} -o ${pictureFilePath}
