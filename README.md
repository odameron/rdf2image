# rdf2image

Utility script for generating a graph representation of a (simple) RDF file.


# Usage

`rdf2image.bash -r <RDF file path> [-p <Picture file path>]`

- RDF file type is inferred from file extension (turtle and trig are supported)
- Picture file path is optional (default is PNG from RDF file path)
- Picture file type is inferred from file extension (PNG, PDF and SVG are supported)


# Todo

- [x] automatically determine the type of RDF ~~(currently only turtle is supported)~~
    - [x] turtle is supported
    - [x] trig is supported
- [x] add option to select the type of image ~~(currently only PNG is supported)~~
    - [x] PNG is supported
    - [x] PDF is supported
    - [x] SVG is supported


# See also

- [RDFVizler](http://rdfvizler.dyreriket.xyz/)

